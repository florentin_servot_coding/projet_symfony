<?php

namespace PlatypusBundle\Controller;

use PlatypusBundle\Form\UserType;
use PlatypusBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class UserController extends Controller
{
  public function indexAction()
  {
    return $this->render('PlatypusBundle:User:index.html.twig', array(
      // ...
    ));
  }

  public function loginAction(Request $request)
  {
      $authUtils = $this->get('security.authentication_utils');
      $error = $authUtils->getLastAuthenticationError();

      $lastUsername = $authUtils->getLastUsername();

      return ($this->render('PlatypusBundle:User:login.html.twig', array(
        'last_username' => $lastUsername,
      )));
  }

  public function registerAction(Request $request, UserPasswordEncoderInterface $factory = null)
  {
    $user = new User();
    $form = $this->createForm(UserType::class, $user);

    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid())
    {
      $user = $form->getData();
      $factory = $this->get('security.password_encoder');
      $password = $factory->encodePassword($user, $user->getPassword());
      $user->setPassword($password);

      $user->setRoles('ROLE_USER');
      $user->setCreationDate(new \DateTime());

      $em = $this->getDoctrine()->getManager();
      $em->persist($user);
      $em->flush();

      return $this->render(
        'PlatypusBundle:User:register.html.twig',
        array(
          'form' => $form->createView(),
          'messageValidation' => "the user " . $user->getFirstName() . " " . $user->getLastName() . " is created!"
      ));
    }

    $error = 'Error: fields incorrect or user already exist.';
    return $this->render(
      'PlatypusBundle:User:register.html.twig',
      array(
        'form' => $form->createView(),
        'formSubmit' => $form->isSubmitted(),
        'error' => $error
    ));
  }

  public function editAction(Request $request)
  {
    $user = new User();
    $form = $this->createForm(UserType::class, $user);

    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid())
    {
      $user = $form->getData();
      $factory = $this->get('security.password_encoder');

      $password = $factory->encodePassword($user, $user->getPassword());
      if (!$password)
        $user->setPassword($user->getPassword());
      else
        $user->setPassword($password);

       $validator = $this->get('validator');
       $errors = $validator->validate($user);
       if (count($errors) > 0)
         $response = (string) $errors;
       else
         $response = array('error' => "the user " . $user->getFirstName() . " " . $user->getLastName() . " is updated! F**K YEAH!!");

       $em = $this->getDoctrine()->getManager();
       $em->persist($user);
       $em->flush();

       return $this->render(
         'PlatypusBundle:User:edit.html.twig',
         array(
           'form' => $form->createView(),
           'response' => $response
      ));
     }
     return $this->render(
       'PlatypusBundle:User:edit.html.twig',
       array(
         'form' => $form->createView(),
       ));
  }
}
